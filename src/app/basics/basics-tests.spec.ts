describe('This is a basic test suite', () => {

    let calculator = {
        plus: (a, b) => a + b,
        multiply: (a, b) => a * b
    }

    interface Calculator {
        plus(a, b): number;
        multiply(a, b): number;
    }

    afterEach(()=> {
        calculator = {
            plus: (a, b) => a + b,
            multiply: (a, b) => a * b
        }
    });

    it('this is a basic unit test', () => {
        expect(true).toBeTruthy();
    })

    it('should sum 1+1=2', () => {
        expect(calculator.plus(1, 1)).toBe(2);
    })

    it('should multiply 4*4=16', () => {
        expect(calculator.multiply(4, 4)).toBe(16)
    })  

    it('should return 10 when plus two numbers', () => {
        spyOn(calculator, "plus").and.returnValue(10);
        expect(calculator.plus(3, 3)).toBe(10);
    })

    it('should return 10 fake calculator', () => {

        let fakeCalculator: Calculator = jasmine.createSpyObj('calculator', {
            "plus": 10
        })

        expect(fakeCalculator.plus(1, 5)).toBe(10);
        expect(fakeCalculator.plus).toHaveBeenCalled();

    })

    it('should spy method multiply and return 10', () => {
        calculator.multiply = jasmine.createSpy('multiply').and.returnValue(10);
        expect(calculator.multiply(1, 2)).toEqual(10);

    })

    it('should spy mehtod sith spyOn and return 10', () => {
        spyOn(calculator,'plus').and.returnValue(10);
        expect(calculator.plus(10,13)).toBe(10);
    })


})